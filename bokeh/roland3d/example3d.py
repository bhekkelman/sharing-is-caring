from __future__ import division

import numpy as np
import pandas as pd

from bokeh.models import Slider
from bokeh.models import ColumnDataSource, LayoutDOM
from bokeh.io import curdoc
from bokeh.layouts import WidgetBox, column, row
from bokeh.plotting import figure
from bokeh.core.properties import Any, Dict, Instance, String

# This defines some default options for the Graph3d feature of vis.js
# See: http://visjs.org/graph3d_examples.html for more details. Note
# that we are fixing the size of this component, in ``options``, but
# with additional work it could be made more responsive.
DEFAULTS = {
    'width':          '600px',
    'height':         '600px',
    'style':          'surface',
    'showPerspective': True,
    'showGrid':        True,
    'keepAspectRatio': True,
    'verticalRatio':   1.0,
    'legendLabel':     'stuff',
    'cameraPosition':  {
        'horizontal': -0.35,
        'vertical':    0.22,
        'distance':    1.8,
    }
}

# This custom extension model will have a DOM view that should layout-able in
# Bokeh layouts, so use ``LayoutDOM`` as the base class. If you wanted to create
# a custom tool, you could inherit from ``Tool``, or from ``Glyph`` if you
# wanted to create a custom glyph, etc.
class Surface3d(LayoutDOM):

    # The special class attribute ``__implementation__`` should contain a string
    # of JavaScript (or TypeScript) code that implements the JavaScript side
    # of the custom extension model.
    __implementation__ = "surface3d.ts"

    # Below are all the "properties" for this model. Bokeh properties are
    # class attributes that define the fields (and their types) that can be
    # communicated automatically between Python and the browser. Properties
    # also support type validation. More information about properties in
    # can be found here:
    #
    #    https://bokeh.pydata.org/en/latest/docs/reference/core.html#bokeh-core-properties

    # This is a Bokeh ColumnDataSource that can be updated in the Bokeh
    # server by Python code
    data_source = Instance(ColumnDataSource)

    # The vis.js library that we are wrapping expects data for x, y, and z.
    # The data will actually be stored in the ColumnDataSource, but these
    # properties let us specify the *name* of the column that should be
    # used for each field.
    x = String

    y = String

    z = String

    # Any of the available vis.js options for Graph3d can be set by changing
    # the contents of this dictionary.
    options = Dict(String, Any, default=DEFAULTS)

def get_dataset(a, b, c):
    """
    subsets the data set and returns it as a 'Datasource'
    """

    c = min(a,max(12-b,min(a,6)))*(a>0)+max(a,min(-12-b,max(a,-6)))*(a<0)
    d = min(b,max(12-a,min(b,6)))*(b>0)+max(b,min(-12-a,max(b,-6)))*(b<0)

    x1a = [a, 0]
    x2a = [a, a] 
    y1a = [0, b]
    y2a = [b, b] 

    x1d = [0, 0]
    x2d = [c, 0] 
    y1d = [0, 0]
    y2d = [d, 0] 

    x1q = [c, 0]
    x2q = [c, c] 
    y1q = [0, d]
    y2q = [d, d] 

    x = np.arange(0, 300, 20)
    y = np.arange(0, 300, 20)
    xx, yy = np.meshgrid(x, y)
    xx = xx.ravel()
    yy = yy.ravel()

    zz = np.sin(xx/50 + a/10) * np.cos(yy/50 + a/10) * 50 + 50

#    print('x:', xx)
#    print('y:', yy)
#    print('z:', zz)

    return ColumnDataSource(dict(x=xx, y=yy, z=zz))
#    return ColumnDataSource(dict(x1a=x1a, x2a=x2a, y1a=y1a, y2a=y2a, x1d=x1d, x2d=x2d, y1d=y1d, y2d=y2d, x1q=x1q, x2q=x2q, y1q=y1q, y2q=y2q))

def update_dataset(attrname, old, new):
    """
    Updates data set
    """
    # read parameters from sliders
    a = Slider_A.value
    b = Slider_B.value
    c = Slider_C.value

    # get the new data source
    new_source = get_dataset(a, b, c)
    Source_Data.data.update(new_source.data)


#============================ Set up slider ====================================
# start by slider for a
Slider_A = Slider(start=-20, end=20, value=0, step=1, title='prosumer a')

# slider for b
Slider_B = Slider(start=-20, end=20, value=0, step=1, title='prosumer b')

# slider for c
Slider_C = Slider(start=-20, end=20, value=0, step=1, title='prosumer c')


#==================== Setting up the plot ======================================
source = get_dataset(0,0,0)

surface = Surface3d(x="x", y="y", z="z", data_source=source)

for slider in [Slider_A, Slider_B, Slider_C]:
    slider.on_change('value', update_dataset)

ControlPanel = WidgetBox(Slider_A, Slider_B, Slider_C)
LAYOUT = row(surface, ControlPanel)

curdoc().add_root(LAYOUT)

curdoc().title = "Surface3d"
