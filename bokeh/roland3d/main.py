import numpy as np

from bokeh.models import Slider
from bokeh.io import curdoc
from bokeh.models import ColumnDataSource
from bokeh.layouts import WidgetBox, column, row

from roland3d import Roland3D


#==================== Manipulating and initializing the data ======================================

def get_dataset(a, b, c):
    """
    subsets the data set and returns it as a 'Datasource'
    """

    x = np.array([1,1,0,0,0,1,1,0,0,1,0,1,0.5,0,1])
    y = np.array([0,1,1,1,0,0,0,1,0,1,1,0,0.5,0,1])
    z = np.array([0,0,0,1,1,1,0,1,1,0,0,1,0.5,0,1])

    return dict(x=x, y=y, z=z)

source_data = ColumnDataSource(get_dataset(0,0,0))


#==================== Setting up the sliders ====================================

slider_1 = Slider(start=-1, end=1, value=0, step=1, title='prosumer a')
slider_2 = Slider(start=-1, end=1, value=0, step=1, title='prosumer b')
slider_3 = Slider(start=-1, end=1, value=0, step=1, title='prosumer c')


#==================== Setting up the plot ======================================

plot = Roland3D(x="x", y="y", z="z", data_source=source_data)

#ThePlot.segment('x1a', 'y1a', 'x2a', 'y2a', source=Source_Data, line_width=2, color ='blue')
#ThePlot.segment('x1d', 'y1d', 'x2d', 'y2d', source=Source_Data, line_width=2, color ='green')
#ThePlot.segment('x1q', 'y1q', 'x2q', 'y2q', source=Source_Data, line_width=1, color ='green', line_dash='dashed')

#x = np.arange(-20, 20, 1)
#y = np.arange(-20, 20, 1)
#xx, yy = np.meshgrid(x, y)
#xx = xx.ravel()
#yy = yy.ravel()
#zz = 12 - xx - yy
#plot.line(x, 12-x, line_width=2, color='red')
#ThePlot.line(x, -12-x, line_width=2, color='red')

#ThePlot.segment([-20, 0], [0,-20], [20,0], [0,20], line_width=1, color='black')


#==================== Making the Java environment check back on the sliders ======================================

def update():
    source_data.data = get_dataset(slider_1.value, slider_2.value, slider_3.value)

curdoc().add_periodic_callback(update, 100)


#==================== Bringing the layout together ======================================

ControlPanel = WidgetBox(slider_1, slider_2, slider_3)
LAYOUT = row(plot, ControlPanel)
curdoc().add_root(LAYOUT)
