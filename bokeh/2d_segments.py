"""
Making an interactive plot with bokeh

Command to run it:
bokeh serve --show  visualize_bokeh.py
"""
# pylint: disable=C0123

import numpy as np
from bokeh.models import Slider
from bokeh.models.sources import ColumnDataSource
from bokeh.io import curdoc
from bokeh.layouts import WidgetBox, column, row
from bokeh.plotting import figure

def get_dataset(a, b, c):
    """
    subsets the data set and returns it as a 'Datasource'
    """

    if c == 0:
        d = min(a,max(12-b,min(a,6)))*(a>0) + max(a,min(-12-b,max(a,-6)))*(a<0)
        e = min(b,max(12-a,min(b,6)))*(b>0) + max(b,min(-12-a,max(b,-6)))*(b<0)

        x1d = [0, 0]
        x2d = [d*(1-(-12<a+b<12))*(1-(a<0<b)-(b<0<a)), 0] 
        y1d = [0, 0]
        y2d = [e*(1-(-12<a+b<12))*(1-(a<0<b)-(b<0<a)), 0] 
    elif c == 1:
        if a+b == 0:
            s = 1
        else:
            s = a+b

        d = a*12/s*((a+b>12)-(a+b<-12))*(1-(a<0<b)-(b<0<a)) + a*((a>0>-12>b)+(a<0<12<b)) + (12-b)*(a>12>0>b) + (-12-b)*(a<-12<0<b)
        e = b*12/s*((a+b>12)-(a+b<-12))*(1-(a<0<b)-(b<0<a)) + b*((b>0>-12>a)+(b<0<12<a)) + (12-a)*(b>12>0>a) + (-12-a)*(b<-12<0<a)

        x1d = [0, 0]
        x2d = [a*(1-(-12<a+b<12))*(1-(a<0<b)-(b<0<a)), 0] 
        y1d = [0, 0]
        y2d = [b*(1-(-12<a+b<12))*(1-(a<0<b)-(b<0<a)), 0] 
    elif c == 2:
        d = min(max(a-max((a+b-12)/2,0),0),max(12,12-b))*(a>0) + max(min(a-min((a+b+12)/2,0),0),min(-12,-12-b))*(a<0)
        e = min(max(b-max((a+b-12)/2,0),0),max(12,12-a))*(b>0) + max(min(b-min((a+b+12)/2,0),0),min(-12,-12-a))*(b<0)

        x1d = [min(d,a)*(a>0)+max(d,a)*(a<0), 0]
        x2d = [a, 0] 
        y1d = [min(e,b)*(b>0)+max(e,b)*(b<0), 0]
        y2d = [b, 0] 

    x1a = [a, 0]
    x2a = [a, a] 
    y1a = [0, b]
    y2a = [b, b] 

    d = d*(1-(-12<=a+b<=12)) + a*(-12<=a+b<=12)
    e = e*(1-(-12<=a+b<=12)) + b*(-12<=a+b<=12)

    x1q = [d, 0]
    x2q = [d, d] 
    y1q = [0, e]
    y2q = [e, e] 

    return ColumnDataSource(dict(x1a=x1a, x2a=x2a, y1a=y1a, y2a=y2a, x1d=x1d, x2d=x2d, y1d=y1d, y2d=y2d, x1q=x1q, x2q=x2q, y1q=y1q, y2q=y2q))

def update_dataset(attrname, old, new):
    """
    Updates data set
    """
    # read parameters from sliders
    a = Slider_A.value
    b = Slider_B.value
    c = Slider_C.value

    # get the new data source
    new_source = get_dataset(a, b, c)
    source_data.data.update(new_source.data)


#============================ Set up slider ====================================
# start by slider for a
Slider_A = Slider(start=-20, end=20, value=0, step=1, title='prosumer a')

# slider for b
Slider_B = Slider(start=-20, end=20, value=0, step=1, title='prosumer b')

# slider for c
Slider_C = Slider(start=0, end=2, value=0, step=1, title='division type')


#==================== Setting up the plot ======================================
# Setting up data source
source_data = get_dataset(0,0,0)

# Making the plot
plot = figure(x_range=(-20,20), y_range=(-20,20), plot_width=500, plot_height=500, title="division",
                 x_axis_label='activity of a', y_axis_label='activity of b')

plot.segment('x1a', 'y1a', 'x2a', 'y2a', source=source_data, line_width=2, color ='blue')
plot.segment('x1q', 'y1q', 'x2q', 'y2q', source=source_data, line_width=1, color ='green')
plot.segment('x1d', 'y1d', 'x2d', 'y2d', source=source_data, line_width=1, color ='green', line_dash='dashed')
plot.circle('x1q','y1q', source=source_data, size=5, line_color='black', line_alpha=1, fill_color='yellow', fill_alpha=1)

x = np.linspace(-20,20,400)
plot.line(x, 12-x, line_width=2, color='red')
plot.line(x, -12-x, line_width=2, color='red')

plot.segment([-20, 0], [0,-20], [20,0], [0,20], line_width=1, color='black')


#==================== Putting it together ======================================
# make the sliders update the graph
for slider in [Slider_A, Slider_B, Slider_C]:
    slider.on_change('value', update_dataset)

# combine sliders into one
ControlPanel = WidgetBox(Slider_A, Slider_B, Slider_C)
# arrange the sliders and plot in a row
LAYOUT = row(plot, ControlPanel)

# Add it to the current document (displays plot) I have no clue what this does
curdoc().add_root(LAYOUT)
