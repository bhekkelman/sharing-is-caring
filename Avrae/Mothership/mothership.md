# Mothership General

## Mothership Alias Usage Help Command
```
!servalias mshelp embed
-title "Some useful Avrae commands for playing Mothership"
-desc "*Arguments preceded by an asterisk (\*) are optional.*{{"\n"}}
!set\_name [name]{{"\n-"}} Sets your character's name to [name].{{"\n"}}
!set\_[statname] [value]{{"\n-"}} Sets your character's [statname] stat (or save) to [value].{{"\n"}}
![statname] [\*bonus] -b [\*bonus] \*adv \*dis{{"\n-"}} Makes a [statname] check. Pass a number to add a bonus (default 0). Negative bonuses must be passed using -b, positive ones can be passed directly. Finish with adv or dis for advantage or disadvantage.{{"\n"}}
!set\_stress [level]{{"\n-"}} Sets your character's stress to [level].{{"\n"}}
!stress [\*amount]{{"\n-"}} Increases your character's stress by [\*amount] (default 1).{{"\n"}}
!destress [\*amount]{{"\n-"}} Decreases your character's stress by [\*amount] (default 1).{{"\n"}}
!mystress{{"\n-"}} Shows your character's current stress level.{{"\n"}}
!panic{{"\n-"}} PANIC!!{{"\n"}}"
-color 0000ff
```

# Setting Character Values

## Set Character Name
```
!servalias set_name embed
{{ arg = list(argparse(&ARGS&))[0] }}
{{ set_uvar("Name", arg) }}
-title "{{get("Name")}} {{f"set their name to, well, {arg}."}}"
```

## Set Character Stress Level
```
!servalias set_stress embed
{{ arg = list(argparse(&ARGS&))[0] }}
{{ set_uvar("Stress", arg) }}
-title "{{get("Name") if uvar_exists("Name") else 'Someone'}} {{f"set their stress level to {arg}."}}"
```

## Set Character Strength Stat
```
!servalias set_strength embed
{{ arg = list(argparse(&ARGS&))[0] }}
{{ set_uvar("Strength", arg) }}
-title "{{get("Name") if uvar_exists("Name") else 'Someone'}} {{f"set their strength stat to {arg}."}}"
```

## Set Character Speed Stat
```
!servalias set_speed embed
{{ arg = list(argparse(&ARGS&))[0] }}
{{ set_uvar("Speed", arg) }}
-title "{{get("Name") if uvar_exists("Name") else 'Someone'}} {{f"set their speed stat to {arg}."}}"
```

## Set Character Intellect Stat
```
!servalias set_intellect embed
{{ arg = list(argparse(&ARGS&))[0] }}
{{ set_uvar("Intellect", arg) }}
-title "{{get("Name") if uvar_exists("Name") else 'Someone'}} {{f"set their intellect stat to {arg}."}}"
```

## Set Character Combat Stat
```
!servalias set_combat embed
{{ arg = list(argparse(&ARGS&))[0] }}
{{ set_uvar("Combat", arg) }}
-title "{{get("Name") if uvar_exists("Name") else 'Someone'}} {{f"set their combat stat to {arg}."}}"
```

## Set Character Sanity Save
```
!servalias set_sanity embed
{{ arg = list(argparse(&ARGS&))[0] }}
{{ set_uvar("Sanity", arg) }}
-title "{{get("Name") if uvar_exists("Name") else 'Someone'}} {{f"set their sanity save to {arg}."}}"
```

## Set Character Fear Save
```
!servalias set_fear embed
{{ arg = list(argparse(&ARGS&))[0] }}
{{ set_uvar("Fear", arg) }}
-title "{{get("Name") if uvar_exists("Name") else 'Someone'}} {{f"set their fear save to {arg}."}}"
```

## Set Character Body Save
```
!servalias set_body embed
{{ arg = list(argparse(&ARGS&))[0] }}
{{ set_uvar("Body", arg) }}
-title "{{get("Name") if uvar_exists("Name") else 'Someone'}} {{f"set their body save to {arg}."}}"
```

## Set Character Armor Save
```
!servalias set_armor embed
{{ arg = list(argparse(&ARGS&))[0] }}
{{ set_uvar("Armor", arg) }}
-title "{{get("Name") if uvar_exists("Name") else 'Someone'}} {{f"set their armor save to {arg}."}}"
```

# Stress and Panic

## Increase Stress Level
```
!servalias stress embed
{{ args = argparse(&ARGS&) }}
{{ arglist = list(args) }}
{{ arglist.append(1) }}
{{ vals = [int(get("Stress")),  int(arglist[0])] }}
{{ set_uvar("Stress", vals[0] + vals[1]) }}
-title "{{get("Name") if uvar_exists("Name") else 'Someone'}}{{f"'s stress level rose from {vals[0]} to {vals[0]+vals[1]}!"}}"
-color fc7e00
```

## Decrease Stress Level
```
!servalias destress embed
{{ args = argparse(&ARGS&) }}
{{ arglist = list(args) }}
{{ arglist.append(1) }}
{{ vals = [int(get("Stress")),  int(arglist[0])] }}
{{ set_uvar("Stress", vals[0] - vals[1]) }}
-title "{{get("Name") if uvar_exists("Name") else 'Someone'}}{{f"'s stress level went down from {vals[0]} to {vals[0]-vals[1]}!"}}"
-color 7efc00
```

## Display Current Stress Level
```
!servalias mystress embed
-title "{{get("Name") if uvar_exists("Name") else 'Someone'}}{{f"'s current stress level is"}} {{get("Stress")}}."
-color fcfc00
```

## Make a Panic Check
```
!servalias panic embed
{{ stress = int(get("Stress")) }}
{{ r = roll('2d10') }}
{{ e = roll('2d10') }}
-title "{{get("Name") if uvar_exists("Name") else 'Someone'}} {{f"makes a stress check... and {'succeeds' if r > stress else 'fails'}!"}}"
-desc "Rolled {{'**'+str(r)+'**'}} vs {{stress}} {{"\n"}} {{'Relieved 1 stress.' if r > stress else 'Panic effect roll: '}}{{set_uvar("Stress", stress-1) if r > stress else f"{e} -> **{e + stress}**"}}"
-color {{'00ff00' if r > stress else 'ff0000'}}
```

# Making Stat Checks

## Make a Strength Check
```
!servalias strength embed
{{ args = argparse(&ARGS&) }}
{{ arglist = list(args) }}
{{ arglist.append(0) }}
{{ adv = args.get('adv',[0],int)[0] }}
{{ dis = args.get('dis',[0],int)[0] }}
{{ arglist.remove('adv') if adv else arglist.remove('dis') if dis else ''}}
{{ bonus = int(f"{arglist[0] if args.get('b') == [] else args.last('b')}") }}
{{ stat = int(get("Strength")) }}
{{ total = stat + bonus }}
{{ ir = [roll('1d100') - 1, roll('1d100') -1] }}
{{ cs = [x for x in ir if not x > total and x in range(0,100,11)] }}
{{ s = [x for x in ir if not x > total and not x in range(0,100,11)] }}
{{ f = [x for x in ir if x > total and not x in range(0,100,11)] }}
{{ cf = [x for x in ir if x > total and x in range(0,100,11)] }}
{{ cs.sort(reverse=adv) }}
{{ s.sort(reverse=adv) }}
{{ f.sort(reverse=dis) }}
{{ cf.sort(reverse=dis) }}
{{ pr  = (cs+s+f+cf)*adv + (cf+f+s+cs)*dis }}
{{ r = int(f"{pr[0] if adv+dis else ir[0]}") }}
-title "{{get("Name") if uvar_exists("Name") else 'Someone'}} {{f"makes a strength check{' with advantage' if adv else ''}{' with disadvantage' if dis else''}... and {'critically ' if r in range(0,100,11) else ''}{'fails' if r > total else 'succeeds'}!"}}"
-desc "Rolled {{str(pr[1])+' & **'+str(r)+'**' if adv+dis else '**'+str(r)+'**'}} vs {{total}} {{'('+str(stat)+f"{'+' if bonus > 0 else ''}"+str(bonus)+')' if bonus != 0 else ''}}"
-color {{'ff00' if r > total else '00ff'}}{{'99'  if r in range(0,100,11) else '00'}}
```

## Make a Speed Check
```
!servalias speed embed
{{ args = argparse(&ARGS&) }}
{{ arglist = list(args) }}
{{ arglist.append(0) }}
{{ adv = args.get('adv',[0],int)[0] }}
{{ dis = args.get('dis',[0],int)[0] }}
{{ arglist.remove('adv') if adv else arglist.remove('dis') if dis else ''}}
{{ bonus = int(f"{arglist[0] if args.get('b') == [] else args.last('b')}") }}
{{ stat = int(get("Speed")) }}
{{ total = stat + bonus }}
{{ ir = [roll('1d100') - 1, roll('1d100') -1] }}
{{ cs = [x for x in ir if not x > total and x in range(0,100,11)] }}
{{ s = [x for x in ir if not x > total and not x in range(0,100,11)] }}
{{ f = [x for x in ir if x > total and not x in range(0,100,11)] }}
{{ cf = [x for x in ir if x > total and x in range(0,100,11)] }}
{{ cs.sort(reverse=adv) }}
{{ s.sort(reverse=adv) }}
{{ f.sort(reverse=dis) }}
{{ cf.sort(reverse=dis) }}
{{ pr  = (cs+s+f+cf)*adv + (cf+f+s+cs)*dis }}
{{ r = int(f"{pr[0] if adv+dis else ir[0]}") }}
-title "{{get("Name") if uvar_exists("Name") else 'Someone'}} {{f"makes a speed check{' with advantage' if adv else ''}{' with disadvantage' if dis else''}... and {'critically ' if r in range(0,100,11) else ''}{'fails' if r > total else 'succeeds'}!"}}"
-desc "Rolled {{str(pr[1])+' & **'+str(r)+'**' if adv+dis else '**'+str(r)+'**'}} vs {{total}} {{'('+str(stat)+f"{'+' if bonus > 0 else ''}"+str(bonus)+')' if bonus != 0 else ''}}"
-color {{'ff00' if r > total else '00ff'}}{{'99'  if r in range(0,100,11) else '00'}}
```

## Make an Intellect Check
```
!servalias intellect embed
{{ args = argparse(&ARGS&) }}
{{ arglist = list(args) }}
{{ arglist.append(0) }}
{{ adv = args.get('adv',[0],int)[0] }}
{{ dis = args.get('dis',[0],int)[0] }}
{{ arglist.remove('adv') if adv else arglist.remove('dis') if dis else ''}}
{{ bonus = int(f"{arglist[0] if args.get('b') == [] else args.last('b')}") }}
{{ stat = int(get("Intellect")) }}
{{ total = stat + bonus }}
{{ ir = [roll('1d100') - 1, roll('1d100') -1] }}
{{ cs = [x for x in ir if not x > total and x in range(0,100,11)] }}
{{ s = [x for x in ir if not x > total and not x in range(0,100,11)] }}
{{ f = [x for x in ir if x > total and not x in range(0,100,11)] }}
{{ cf = [x for x in ir if x > total and x in range(0,100,11)] }}
{{ cs.sort(reverse=adv) }}
{{ s.sort(reverse=adv) }}
{{ f.sort(reverse=dis) }}
{{ cf.sort(reverse=dis) }}
{{ pr  = (cs+s+f+cf)*adv + (cf+f+s+cs)*dis }}
{{ r = int(f"{pr[0] if adv+dis else ir[0]}") }}
-title "{{get("Name") if uvar_exists("Name") else 'Someone'}} {{f"makes an intellect check{' with advantage' if adv else ''}{' with disadvantage' if dis else''}... and {'critically ' if r in range(0,100,11) else ''}{'fails' if r > total else 'succeeds'}!"}}"
-desc "Rolled {{str(pr[1])+' & **'+str(r)+'**' if adv+dis else '**'+str(r)+'**'}} vs {{total}} {{'('+str(stat)+f"{'+' if bonus > 0 else ''}"+str(bonus)+')' if bonus != 0 else ''}}"
-color {{'ff00' if r > total else '00ff'}}{{'99'  if r in range(0,100,11) else '00'}}
```

## Make a Combat Check
```
!servalias combat embed
{{ args = argparse(&ARGS&) }}
{{ arglist = list(args) }}
{{ arglist.append(0) }}
{{ adv = args.get('adv',[0],int)[0] }}
{{ dis = args.get('dis',[0],int)[0] }}
{{ arglist.remove('adv') if adv else arglist.remove('dis') if dis else ''}}
{{ bonus = int(f"{arglist[0] if args.get('b') == [] else args.last('b')}") }}
{{ stat = int(get("Combat")) }}
{{ total = stat + bonus }}
{{ ir = [roll('1d100') - 1, roll('1d100') -1] }}
{{ cs = [x for x in ir if not x > total and x in range(0,100,11)] }}
{{ s = [x for x in ir if not x > total and not x in range(0,100,11)] }}
{{ f = [x for x in ir if x > total and not x in range(0,100,11)] }}
{{ cf = [x for x in ir if x > total and x in range(0,100,11)] }}
{{ cs.sort(reverse=adv) }}
{{ s.sort(reverse=adv) }}
{{ f.sort(reverse=dis) }}
{{ cf.sort(reverse=dis) }}
{{ pr  = (cs+s+f+cf)*adv + (cf+f+s+cs)*dis }}
{{ r = int(f"{pr[0] if adv+dis else ir[0]}") }}
-title "{{get("Name") if uvar_exists("Name") else 'Someone'}} {{f"makes a combat check{' with advantage' if adv else ''}{' with disadvantage' if dis else''}... and {'critically ' if r in range(0,100,11) else ''}{'fails' if r > total else 'succeeds'}!"}}"
-desc "Rolled {{str(pr[1])+' & **'+str(r)+'**' if adv+dis else '**'+str(r)+'**'}} vs {{total}} {{'('+str(stat)+f"{'+' if bonus > 0 else ''}"+str(bonus)+')' if bonus != 0 else ''}}"
-color {{'ff00' if r > total else '00ff'}}{{'99'  if r in range(0,100,11) else '00'}}
```

# Making Saves

## Make a Sanity Save
```
!servalias sanity embed
{{ args = argparse(&ARGS&) }}
{{ arglist = list(args) }}
{{ arglist.append(0) }}
{{ adv = args.get('adv',[0],int)[0] }}
{{ dis = args.get('dis',[0],int)[0] }}
{{ arglist.remove('adv') if adv else arglist.remove('dis') if dis else ''}}
{{ bonus = int(f"{arglist[0] if args.get('b') == [] else args.last('b')}") }}
{{ stat = int(get("Sanity")) }}
{{ total = stat + bonus }}
{{ ir = [roll('1d100') - 1, roll('1d100') -1] }}
{{ cs = [x for x in ir if not x > total and x in range(0,100,11)] }}
{{ s = [x for x in ir if not x > total and not x in range(0,100,11)] }}
{{ f = [x for x in ir if x > total and not x in range(0,100,11)] }}
{{ cf = [x for x in ir if x > total and x in range(0,100,11)] }}
{{ cs.sort(reverse=adv) }}
{{ s.sort(reverse=adv) }}
{{ f.sort(reverse=dis) }}
{{ cf.sort(reverse=dis) }}
{{ pr  = (cs+s+f+cf)*adv + (cf+f+s+cs)*dis }}
{{ r = int(f"{pr[0] if adv+dis else ir[0]}") }}
-title "{{get("Name") if uvar_exists("Name") else 'Someone'}} {{f"makes a sanity save{' with advantage' if adv else ''}{' with disadvantage' if dis else''}... and {'critically ' if r in range(0,100,11) else ''}{'fails' if r > total else 'succeeds'}!"}}"
-desc "Rolled {{str(pr[1])+' & **'+str(r)+'**' if adv+dis else '**'+str(r)+'**'}} vs {{total}} {{'('+str(stat)+f"{'+' if bonus > 0 else ''}"+str(bonus)+')' if bonus != 0 else ''}}"
-color {{'ff00' if r > total else '00ff'}}{{'99'  if r in range(0,100,11) else '00'}}
```

## Make a Fear Save
```
!servalias fear embed
{{ args = argparse(&ARGS&) }}
{{ arglist = list(args) }}
{{ arglist.append(0) }}
{{ adv = args.get('adv',[0],int)[0] }}
{{ dis = args.get('dis',[0],int)[0] }}
{{ arglist.remove('adv') if adv else arglist.remove('dis') if dis else ''}}
{{ bonus = int(f"{arglist[0] if args.get('b') == [] else args.last('b')}") }}
{{ stat = int(get("Fear")) }}
{{ total = stat + bonus }}
{{ ir = [roll('1d100') - 1, roll('1d100') -1] }}
{{ cs = [x for x in ir if not x > total and x in range(0,100,11)] }}
{{ s = [x for x in ir if not x > total and not x in range(0,100,11)] }}
{{ f = [x for x in ir if x > total and not x in range(0,100,11)] }}
{{ cf = [x for x in ir if x > total and x in range(0,100,11)] }}
{{ cs.sort(reverse=adv) }}
{{ s.sort(reverse=adv) }}
{{ f.sort(reverse=dis) }}
{{ cf.sort(reverse=dis) }}
{{ pr  = (cs+s+f+cf)*adv + (cf+f+s+cs)*dis }}
{{ r = int(f"{pr[0] if adv+dis else ir[0]}") }}
-title "{{get("Name") if uvar_exists("Name") else 'Someone'}} {{f"makes a fear save{' with advantage' if adv else ''}{' with disadvantage' if dis else''}... and {'critically ' if r in range(0,100,11) else ''}{'fails' if r > total else 'succeeds'}!"}}"
-desc "Rolled {{str(pr[1])+' & **'+str(r)+'**' if adv+dis else '**'+str(r)+'**'}} vs {{total}} {{'('+str(stat)+f"{'+' if bonus > 0 else ''}"+str(bonus)+')' if bonus != 0 else ''}}"
-color {{'ff00' if r > total else '00ff'}}{{'99'  if r in range(0,100,11) else '00'}}
```

## Make a Body Save
```
!servalias body embed
{{ args = argparse(&ARGS&) }}
{{ arglist = list(args) }}
{{ arglist.append(0) }}
{{ adv = args.get('adv',[0],int)[0] }}
{{ dis = args.get('dis',[0],int)[0] }}
{{ arglist.remove('adv') if adv else arglist.remove('dis') if dis else ''}}
{{ bonus = int(f"{arglist[0] if args.get('b') == [] else args.last('b')}") }}
{{ stat = int(get("Body")) }}
{{ total = stat + bonus }}
{{ ir = [roll('1d100') - 1, roll('1d100') -1] }}
{{ cs = [x for x in ir if not x > total and x in range(0,100,11)] }}
{{ s = [x for x in ir if not x > total and not x in range(0,100,11)] }}
{{ f = [x for x in ir if x > total and not x in range(0,100,11)] }}
{{ cf = [x for x in ir if x > total and x in range(0,100,11)] }}
{{ cs.sort(reverse=adv) }}
{{ s.sort(reverse=adv) }}
{{ f.sort(reverse=dis) }}
{{ cf.sort(reverse=dis) }}
{{ pr  = (cs+s+f+cf)*adv + (cf+f+s+cs)*dis }}
{{ r = int(f"{pr[0] if adv+dis else ir[0]}") }}
-title "{{get("Name") if uvar_exists("Name") else 'Someone'}} {{f"makes a body save{' with advantage' if adv else ''}{' with disadvantage' if dis else''}... and {'critically ' if r in range(0,100,11) else ''}{'fails' if r > total else 'succeeds'}!"}}"
-desc "Rolled {{str(pr[1])+' & **'+str(r)+'**' if adv+dis else '**'+str(r)+'**'}} vs {{total}} {{'('+str(stat)+f"{'+' if bonus > 0 else ''}"+str(bonus)+')' if bonus != 0 else ''}}"
-color {{'ff00' if r > total else '00ff'}}{{'99'  if r in range(0,100,11) else '00'}}
```

## Make an Armor Save
```
!servalias armor embed
{{ args = argparse(&ARGS&) }}
{{ arglist = list(args) }}
{{ arglist.append(0) }}
{{ adv = args.get('adv',[0],int)[0] }}
{{ dis = args.get('dis',[0],int)[0] }}
{{ arglist.remove('adv') if adv else arglist.remove('dis') if dis else ''}}
{{ bonus = int(f"{arglist[0] if args.get('b') == [] else args.last('b')}") }}
{{ stat = int(get("Armor")) }}
{{ total = stat + bonus }}
{{ ir = [roll('1d100') - 1, roll('1d100') -1] }}
{{ cs = [x for x in ir if not x > total and x in range(0,100,11)] }}
{{ s = [x for x in ir if not x > total and not x in range(0,100,11)] }}
{{ f = [x for x in ir if x > total and not x in range(0,100,11)] }}
{{ cf = [x for x in ir if x > total and x in range(0,100,11)] }}
{{ cs.sort(reverse=adv) }}
{{ s.sort(reverse=adv) }}
{{ f.sort(reverse=dis) }}
{{ cf.sort(reverse=dis) }}
{{ pr  = (cs+s+f+cf)*adv + (cf+f+s+cs)*dis }}
{{ r = int(f"{pr[0] if adv+dis else ir[0]}") }}
-title "{{get("Name") if uvar_exists("Name") else 'Someone'}} {{f"makes an armor save{' with advantage' if adv else ''}{' with disadvantage' if dis else''}... and {'critically ' if r in range(0,100,11) else ''}{'fails' if r > total else 'succeeds'}!"}}"
-desc "Rolled {{str(pr[1])+' & **'+str(r)+'**' if adv+dis else '**'+str(r)+'**'}} vs {{total}} {{'('+str(stat)+f"{'+' if bonus > 0 else ''}"+str(bonus)+')' if bonus != 0 else ''}}"
-color {{'ff00' if r > total else '00ff'}}{{'99'  if r in range(0,100,11) else '00'}}
```