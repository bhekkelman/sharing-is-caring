# Fighter General

## Hitdice
```
!alias hitdice embed
<drac2>
die = "d10"
args = argparse(&ARGS&)
arglist = list(args)
arglist.append(1)
numdice = int(arglist[0])
cc = "Hit Dice"
enough = (character().cc_exists(cc) and character().get_cc(cc) >= numdice)
character().mod_cc(cc, -numdice) if enough else 0
conbonus = numdice * constitutionMod
rollstring = (f"{numdice}{die}+{conbonus}" if enough else 0)
healroll = vroll(rollstring)
preheal = character().hp
character().modify_hp(healroll.total, overflow=False)
postheal = character().hp
heal = postheal - preheal

output = f'-title "{name} {"uses" if enough else "tries to use"} {numdice} hit di{"c" if not numdice==1 else ""}e." -desc "{healroll if enough else ""}' + ("\n" if enough else "") + f'{"But" if not enough else "**"} {name} {"recovers" if enough else "does not have enough hit dice available."} {heal if enough else ""} {"hp.**" if  enough else ""}"  -footer "{name}: {character().hp_str()}' + "\n" + f'{cc}: {character().cc_str(cc)}" -thumb {image}'

return output
</drac2>
```

## +2 Greatsword attack
```
!alias sword embed
<drac2>
a = argparse(&ARGS&)
target = a.last("t")
gm = a.last("gm")
hitrollstring = ["1d20", "2d20kh1", "2d20kl1"][a.adv()] + f'+{strengthMod + proficiencyBonus + 2}'
hitroll = vroll(hitrollstring)
crit = ((hitroll.total - strengthMod - proficiencyBonus - 2) == 20)
gmdmg = (["+1d8ro<3", "+2d8ro<3"][crit] if gm else "")
dmgrollstring = ["2d6ro<3"+f'{gmdmg}', "5d6ro<3"+f'{gmdmg}'][crit]+f'+{strengthMod + 2} [magical slashing]'
dmgroll = vroll(dmgrollstring)

combatTarget = ""
targetname = "something"
hits = True
if combat() and target:
    combatTarget = combat().get_combatant(target)
    targetname = combatTarget.name
    hits = (hitroll.total >= combatTarget.ac)
    dmgroll = (combatTarget.damage(f'2d6ro<3{"+1d8ro<3" if gm else ""} [magical slashing]', crit=crit, d=(strengthMod + 2), critdice=1)['damage'] if hits else "")

gmtext = (" using Giant's Might" if gm else "")
advtext = (" at advantage" if a.adv()==1 else " at disadvantage" if a.adv() == -1 else "")
dmgtext = (f'**Damage{" (CRIT!)" if crit else ""}**: ' if not combatTarget else "")

output = f'-title "{name} attacks {targetname} with his +2 greatsword{gmtext}{advtext}." -desc "**To hit**: {hitroll}' + "\n" + (f'{dmgtext}{dmgroll}' if hits else f'**{name} misses!**') + (f'" -footer "{targetname}: {combatTarget.hp_str()}' if combatTarget else "") + f'" -thumb https://c1.scryfall.com/file/scryfall-cards/art_crop/front/6/3/63b4041d-7c95-4cb9-a18b-6568db05942b.jpg'

return output
</drac2>
```

## Second Wind
```
!alias wind embed
<drac2>
cc = "Second Wind"
enough = (character().cc_exists(cc) and character().get_cc(cc) > 0)
character().mod_cc(cc, -1) if enough else 0
rollstring = (f"1d10+{FighterLevel}" if enough else 0)
healroll = vroll(rollstring)
preheal = character().hp
character().modify_hp(healroll.total, overflow=False)
postheal = character().hp
heal = postheal - preheal

output = f'-title "{name} {"draws" if enough else "tries to draw"} on his {cc}!" -desc "' + (f'{healroll} \n You have a limited well of stamina that you can draw on to protect yourself from harm. On your turn, you can use a bonus action to regain **{heal}** hit points.' if enough else f'But {name} '+"doesn't have any stamina left to draw on.") +  f'" -footer "{name}: {character().hp_str()}' + "\n" + f'{cc}: {character().cc_str(cc)}" -thumb {image}'

return output
</drac2>
```

## Action Surge
```
!alias surge embed
<drac2>
cc = "Action Surge"
enough = (character().cc_exists(cc) and character().get_cc(cc) > 0)
character().mod_cc(cc, -1) if enough else 0

output = f'-title "{name} {"uses" if enough else "tries to use"} his {cc}!" -desc "' + ("You can push yourself beyond your normal limits for a moment. On your turn, you can take one additional action." if enough else f'But {name} is already at his limit.') +  f'" -footer "{cc}: {character().cc_str(cc)}" -thumb {image}'

return output
</drac2>
```

## Indomitable
```
!alias indomitable embed
<drac2>
cc = "Indomitable"
enough = (character().cc_exists(cc) and character().get_cc(cc) > 0)
character().mod_cc(cc, -1) if enough else 0

output = f'-title "{name} {"uses" if enough else "tries to use"} {cc}!" -desc "' + ("You can reroll a saving throw that you fail. If you do so, you must use the new roll." if enough else f'But {name} has already used it.') +  f'" -footer "{cc}: {character().cc_str(cc)}" -thumb {image}'

return output
</drac2>
```

# Rune Knight Specific

## Giant's Might
```
!alias giant embed
<drac2>
cc = "Giant's Might"
usable = (character().cc_exists(cc) and character().get_cc(cc) > 0)
character().mod_cc(cc, -1) if usable else 0

combatTarget = ""
if combat() and combat().me:
    combatTarget = combat().me
    combatTarget.remove_effect(cc)
    combatTarget.add_effect(cc, "", duration=10, desc="Large, advantage on Strength, extra 1d8 damage per turn.")

output = f'-title "{name} {"" if usable else "tries to "}imbue{"s" if usable else ""} himself with the might of giants!" -desc "' + ("- If you are smaller than Large, you become Large, along with anything you are wearing. If you lack the room to become Large, your size doesn't change." if usable else f'But {name}' + " doesn't have any uses of Giant's Might left.")  + (f'\n - You have advantage on Strength checks and Strength saving throws.\n - Once on each of your turns, one of your attacks with a weapon or an unarmed strike can deal an extra 1d8 damage to a target on a hit."' if usable else '"') + f' -footer "{cc}: {character().cc_str(cc)}' + (f'\n{combatTarget.name}|**Effect**: {combatTarget.get_effect(cc)}'  if combatTarget else "") + f'" -thumb https://c1.scryfall.com/file/scryfall-cards/art_crop/front/8/7/875a20c2-1d17-46ea-b4d2-3e70bc05aae3.jpg'

return output
</drac2>
```

## Runic Shield
```
!alias shield embed
<drac2>
cc = "Runic Shield"
available = (character().get_cc(cc) > 0)
character().mod_cc(cc, -1)

output = f'-title "{name} {"invokes" if available else "tries to invoke"} his rune magic to protect his allies!" -desc "' + ("When another creature you can see within 60 feet of you is hit by an attack roll, you can use your reaction to force the attacker to reroll the d20 and use the new roll." if available else "But has no more magic left.") + f'" -footer "{cc}: {character().cc_str(cc)}" -thumb https://c1.scryfall.com/file/scryfall-cards/art_crop/front/0/e/0e31b716-f325-445a-9098-9ca75d7b35a4.jpg?1614985179'

return output
</drac2>
```

## Cloud Rune
```
!alias cloud embed
<drac2>
cc = "Cloud Rune"
available = (character().get_cc(cc) > 0)
character().mod_cc(cc, -1)

output = f'-title "{name} {"invokes" if available else "tries to invoke"} his cloud rune." -desc "' + ("When you or a creature you can see within 30 feet of you is hit by an attack roll, you can use your reaction to invoke the rune and choose a different creature within 30 feet of you, other than the attacker. The chosen creature becomes the target of the attack, using the same roll. This magic can transfer the attack's effects regardless of the attack's range." if available else "But has already used it.") + f'" -footer "{cc}: {character().cc_str(cc)}" -thumb https://c1.scryfall.com/file/scryfall-cards/art_crop/front/e/e/ee39da13-4b8a-4796-a7c2-aaa11992d573.jpg'

return output
</drac2>
```

## Fire Rune
```
!alias fire embed
<drac2>
cc = "Fire Rune"
available = (character().get_cc(cc) > 0)
character().mod_cc(cc, -1)

output = f'-title "{name} {"invokes" if available else "tries to invoke"} his fire rune." -desc "' + ("When you hit a creature with an attack using a weapon, you can invoke the rune to summon fiery shackles: the target takes an extra 2d6 fire damage, and it must succeed on a DC " if available else "But has already used it.") + f'{8+proficiencyBonus+constitutionMod if available else ""}' + (" Strength saving throw or be restrained for 1 minute. While restrained by the shackles, the target takes 2d6 fire damage at the start of each of its turns. The target can repeat the saving throw at the end of each of its turns, banishing the shackles on a success." if available else "") + f'" -footer "{cc}: {character().cc_str(cc)}" -thumb https://c1.scryfall.com/file/scryfall-cards/art_crop/front/8/e/8e9c153c-9224-491b-bc84-8a9f0a83ee5a.jpg'

return output
</drac2>
```

## Hill Rune
```
!alias hill embed
<drac2>
cc = "Hill Rune"
available = (character().get_cc(cc) > 0)
character().mod_cc(cc, -1)

combatTarget = ""
if combat() and combat().me:
    combatTarget = combat().me
    combatTarget.remove_effect(cc)
    combatTarget.add_effect(cc, "", duration=10, desc="Resistance to bludgeoning, piercing, and slashing damage.")

output = f'-title "{name} {"invokes" if available else "tries to invoke"} his hill rune." -desc "' + ("You can invoke the rune as a bonus action, gaining resistance to bludgeoning, piercing, and slashing damage for 1 minute." if available else "But has already used it.") + f'" -footer "{cc}: {character().cc_str(cc)}' + (f'\n{combatTarget.name}|**Effect**: {combatTarget.get_effect(cc)}'  if combatTarget else "") + f'" -thumb https://c1.scryfall.com/file/scryfall-cards/art_crop/front/b/a/ba1384e5-d140-4074-9548-250af09cb413.jpg'

return output
</drac2>
```

## Storm Rune
```
!alias storm embed
<drac2>
cc = "Storm Rune"
available = (character().get_cc(cc) > 0)
character().mod_cc(cc, -1)

output = f'-title "{name} {"invokes" if available else "tries to invoke"} his storm rune." -desc "' + ("You can invoke the rune as a bonus action to enter a prophetic state for 1 minute or until you're incapacitated. Until the state ends, when you or another creature you can see within 60 feet of you makes an attack roll, a saving throw, or an ability check, you can use your reaction to cause the roll to have advantage or disadvantage." if available else "But has already used it.") + f'" -footer "{cc}: {character().cc_str(cc)}" -thumb https://c1.scryfall.com/file/scryfall-cards/art_crop/front/3/3/332bfce9-052d-42e9-a407-4a1dd59e0f2a.jpg'

return output
</drac2>
```
