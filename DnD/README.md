Everything you need to know is in the Player's Handbook. Part 1 is about creating a character, and that's really all you need to know for now. You can read the rest but it's not necessary; I'll catch you up on the most important mechanics (spoiler: it's rolling a lot of dice).

On page 11 starts a little guide on how to create a character. This is a fine guide, but some things are more important than others so I'll rehash those here. Basically it goes as follows:

1. Pick a class (I know race comes first, but class is what really defines your character the most).
2. Pick a race and background (these both have bonuses relevant to your stats).
3. Determine your main stats (ability scores). We'll be using the point system described in the bottom left of page 13 (don't forget to add your racial bonuses afterwards).
4. Pick whatever other things need picking (starting gear, spells, subclasses, etc.). Only starting equipment, no buying with gold.
5. Fill in the first page of the character sheet.
6. There's a bunch more stuff like weight, height, alignment, ideals, flaws, etc. that you can fill out the second page of the sheet with. These probably won't impact gameplay too much so don't bother putting too much time in. I suggest rolling them randomly as described in the book, makes for a fun challenge of imagining who your character is.

## Important stuff to note:

1. We'll start at level 3 so every class gets their subclasses. No need to bother looking at what you get past level 5.
2. All of chapter 6 (customization) is off limits for now.
3. Don't be a murder hobo. New player security will be in place: you're not allowed to have an evil nor chaotic alignment.
4. Spell classes take significantly more effort, since there are so many spells. If you do go for this, somehow print out a list of the spells you've chosen so you don't always have to flip through the book/pdf to look them up.
5. Print out your character sheet and get your hands on some dice.
6. You may want to also print out the pages for your class, race, and background.

## Preparing for the game:

1. Think of a reason why you showed up in the regional capital to accept a (paid) quest, not without peril, set out by the duke, for the public good.

## More on spellcasters:

1. Spells will tell you that you need ingredients or a spellcasting focus. Don't worry about this. Every spellcaster starts with some kind of item that functions as a focus, which means you can ignore all the ingredients (M). Whether a spell requires you to move your hands (S) or speak (V) may come up during gameplay of course.
2. There's a technical difference between spells you know and spells that you have prepared. Basically it won't matter and you can probably consider all of your spells to be prepared (read the class description though). During play you can only cast prepared spells. Knowing a spell just means you can swap it in for one of your currently prepared spells when taking a long rest. But like I said, usually you just have your set of prepared spells and that's what you use.
3. Not every class can use every spell. Check the list to see which ones your class is allowed to pick.
